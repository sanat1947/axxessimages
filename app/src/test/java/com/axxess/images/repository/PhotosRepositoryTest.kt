package com.axxess.images.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.axxess.pixabay.util.TestUtil
import com.axxess.pixabay.util.mock
import com.axxess.images.database.PhotoDoa
import com.axxess.images.database.AxxessDatabase
import com.axxess.images.models.Photo
import com.axxess.images.models.PhotoSearchResult
import com.axxess.images.models.Resource
import com.axxess.images.api.ApiResponse
import com.axxess.images.api.PhotoSearchResponse
import com.axxess.images.api.AxxessService
import com.axxess.images.util.InstantAppExecutors
import com.axxess.images.utils.Constants
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import retrofit2.Response

class PhotosRepositoryTest {
    private lateinit var repository: PhotosRepository
    private val dao = mock(PhotoDoa::class.java)
    private val service = mock(AxxessService::class.java)

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        val db = mock (AxxessDatabase::class.java)
        `when`(db.photoDao()).thenReturn(dao)
        `when`(db.runInTransaction(ArgumentMatchers.any())).thenCallRealMethod()
        repository = PhotosRepository(InstantAppExecutors(), db, dao, service)
    }

    @Test
    fun searchPhotos() {

        val ids = arrayListOf("abc", "abc2")
        val photo1 = TestUtil.createPhoto(1)
        val photo2 = TestUtil.createPhoto(2)

        val observer = mock<Observer<Resource<List<Photo>>>>()
        val dbSearchResult = MutableLiveData<PhotoSearchResult>()
        val photos = MutableLiveData<List<Photo>>()

        val photoList = arrayListOf(photo1, photo2)
        val apiResponse = PhotoSearchResponse(photoList)

        // #1
        val callLiveData = MutableLiveData<ApiResponse<PhotoSearchResponse>>()
        `when`(service.searchPhotos("friends", Constants.AUTHORIZATION)).thenReturn(callLiveData)

        // #2
        `when`(dao.search("friends", 1)).thenReturn(dbSearchResult)


        repository.search("friends", 1).observeForever(observer)


        verify(observer).onChanged(Resource.loading(null))
        verifyNoMoreInteractions(service)
        reset(observer)

        doReturn(photos).`when`(dao).loadOrdered(ids)

        dbSearchResult.postValue(null)
        verify(dao, never()).loadOrdered(anyList())

        verify(service).searchPhotos("friends", Constants.AUTHORIZATION)
        val updatedResult = MutableLiveData<PhotoSearchResult>()
        `when`(dao.search("friends", 1)).thenReturn(updatedResult)
        updatedResult.postValue(PhotoSearchResult("Test", ids, 1))

        callLiveData.postValue(ApiResponse.create(Response.success(apiResponse)))
        verify(dao).insertPhotos(photoList)
        photos.postValue(photoList)
        verify(observer).onChanged(Resource.success(photoList))
        verifyNoMoreInteractions(service)

    }
}