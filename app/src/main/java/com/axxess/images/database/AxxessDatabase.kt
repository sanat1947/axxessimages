package com.axxess.images.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.axxess.images.models.ImageTypeConverter
import com.axxess.images.models.Photo
import com.axxess.images.models.PhotoSearchResult


@Database(
    entities = [
        Photo::class,
        PhotoSearchResult::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class, ImageTypeConverter::class)
abstract class AxxessDatabase : RoomDatabase() {
    abstract fun photoDao() : PhotoDoa
}