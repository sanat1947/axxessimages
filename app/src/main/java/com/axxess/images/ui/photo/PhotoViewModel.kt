package com.axxess.images.ui.photo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.axxess.images.models.Photo
import com.axxess.images.repository.PhotoRepository
import javax.inject.Inject

class PhotoViewModel @Inject constructor(private val photoRepository: PhotoRepository) :
    ViewModel() {

    private val _photoId = MutableLiveData<String>()

    val photo: LiveData<Photo> = Transformations
        .switchMap(_photoId) { id ->
            photoRepository.loadPhotoById(id)
        }


    fun setId(photoId: String) {
//        if (_photoId.value == photoId){
//            return
//        }
        _photoId.value = photoId

    }

}