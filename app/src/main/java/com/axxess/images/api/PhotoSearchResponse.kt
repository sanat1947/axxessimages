package com.axxess.images.api

import com.google.gson.annotations.SerializedName
import com.axxess.images.models.Photo

class PhotoSearchResponse(
    @SerializedName("data")
    val photos: List<Photo>
)