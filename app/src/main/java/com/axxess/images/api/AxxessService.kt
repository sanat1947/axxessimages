package com.axxess.images.api

import androidx.lifecycle.LiveData
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface AxxessService {
    @GET("1")
    fun searchPhotos (
        @Query("q") qery: String,
        @Header("Authorization") authorization: String
        ) : LiveData<ApiResponse<PhotoSearchResponse>> // it can be Flowable when Using RX-Java2
}