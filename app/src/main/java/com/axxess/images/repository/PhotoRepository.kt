package com.axxess.images.repository

import androidx.lifecycle.LiveData
import com.axxess.images.database.PhotoDoa
import com.axxess.images.models.Photo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PhotoRepository @Inject constructor(
    private val photoDao: PhotoDoa
) {
    fun loadPhotoById(photoId: String): LiveData<Photo> {
        return photoDao.getPhotoById(photoId)
    }
}