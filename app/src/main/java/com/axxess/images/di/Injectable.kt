package com.axxess.images.di

/**
 * Marks an activity / fragment injectable.
 * Same as Marker interface in java no fields or methods
 */
interface Injectable