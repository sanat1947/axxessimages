package com.axxess.images.di

import android.app.Application
import androidx.room.Room
import com.axxess.images.database.AxxessDatabase
import com.axxess.images.database.PhotoDoa
import com.axxess.images.api.AxxessService
import com.axxess.images.utils.Constants
import com.axxess.images.utils.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideAxxessService(): AxxessService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(AxxessService::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): AxxessDatabase {
        return Room
            .databaseBuilder(app, AxxessDatabase::class.java, "Axxess.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun providePhotoDao(db: AxxessDatabase): PhotoDoa {
        return db.photoDao()
    }
}