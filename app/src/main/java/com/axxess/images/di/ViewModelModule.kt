package com.axxess.images.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.axxess.images.ui.photo.PhotoViewModel
import com.axxess.images.ui.search.SearchPhotoViewModel
import com.axxess.images.viewmodels.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SearchPhotoViewModel::class)
    abstract fun bindSearchPhotoViewModel(searchPhotoViewModel: SearchPhotoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PhotoViewModel::class)
    abstract fun bindPhotoViewModel(photoViewModel: PhotoViewModel): ViewModel


    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory
}