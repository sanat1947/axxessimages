package com.axxess.images.utils

class Constants {
    companion object {
        const val AUTHORIZATION = "Client-ID 137cda6b5008a7c"
        const val BASE_URL = "https://api.imgur.com/3/gallery/search/"
    }
}