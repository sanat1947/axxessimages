package com.axxess.images.common

/**
 * Generic interface for retry buttons.
 */
interface RetryCallback {
    fun retry()
}