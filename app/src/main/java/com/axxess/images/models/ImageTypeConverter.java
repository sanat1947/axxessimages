package com.axxess.images.models;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class ImageTypeConverter implements Serializable {

    @TypeConverter // note this annotation
    public String fromImageList(List<Image> imageList) {
        if (imageList == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Image>>() {
        }.getType();
        String json = gson.toJson(imageList, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<Image> toImageList(String imageListString) {
        if (imageListString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Image>>() {
        }.getType();
        List<Image> imageList = gson.fromJson(imageListString, type);
        return imageList;
    }

}