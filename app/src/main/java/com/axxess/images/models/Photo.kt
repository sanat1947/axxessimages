package com.axxess.images.models

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(
    indices = [Index("id")]
)
data class Photo(
    @PrimaryKey
    val id: String,

    @SerializedName("title")
    val title: String?,

    @SerializedName("images")
    @TypeConverters(ImageTypeConverter::class)
    val images: List<Image>?

)