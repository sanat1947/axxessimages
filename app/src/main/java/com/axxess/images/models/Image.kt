package com.axxess.images.models

import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("link")
    val link: String)