package com.axxess.images.models

import androidx.room.Entity

@Entity(primaryKeys = ["query", "pageNumber"])
data class PhotoSearchResult(
    val query: String,
    val photoIds: List<String>,
    val pageNumber: Int
)